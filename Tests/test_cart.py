import pytest
import sys
from pages.main_page import MainPage
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

link = "https://rozetka.com.ua/"

class TestCart():
    #@pytest.mark.smoke
    def test_open_cart(self, browser):
        page = MainPage(browser, link)
        page.open()
        cart_btn = page.browser.find_element_by_css_selector('button[opencart]')

        cart_btn.click()
        time.sleep(4)
        assert page.is_element_present('h3[class="modal__heading"]'), 'Couldn\'t load cart window'

    @pytest.mark.smoke
    def test_item_added_to_cart(self, browser):
        page = MainPage(browser, link)
        page.open()
        page.add_to_cart()
        # item = page.browser.find_element_by_css_selector("a[class='tile__title']")
        # item.click()
        # time.sleep(5)
        # buy_btn = page.browser.find_element_by_css_selector('button[class="buy-button button button_with_icon button_color_green button_size_large ng-star-inserted"]')
        # buy_btn.click()
        time.sleep(5)
        assert page.is_element_present('h3[class="modal__heading"]'), 'Couldn\'t load cart window'
        assert len(page.browser.find_elements_by_css_selector('li[class="cart-list__item ng-star-inserted"]')) >=1, "No element was added!"

    #@pytest.mark.smoke
    def test_items_counter_equality_navbar_sidebar(self, browser):
        page = MainPage(browser, link)
        page.add_to_cart(1)
        navbar_items_counter = WebDriverWait(page.browser, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                        'span[class="counter counter--green ng-star-inserted"]'))).text
        #close_cart_window = WebDriverWait(page.browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                                  # '[class="modal__close ng-star-inserted"]'))).click()
        open_side_bar = WebDriverWait(page.browser, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                                                  '[class="header__button"]'))).click()
        sidebar_items_counter = WebDriverWait(page.browser, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR,\
                    'span[class="side-menu__counter side-menu__counter--green ng-tns-c5-0 ng-star-inserted"]'))).text
        #time.sleep(10)
        #print(navbar_items_counter)
        #print(sidebar_items_counter)
        assert navbar_items_counter == sidebar_items_counter, "Side bar items counter and nav bar items counter don't " \
                                                              "match"

    @pytest.mark.new
    def test_user_can_add_more_items_to_cart(self, browser):
        page = MainPage(browser, link)
        page.add_to_cart(8)
        navbar_items_counter = WebDriverWait(page.browser, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                                                                     'span[class="counter counter--green ng-star-inserted"]'))).text
        open_side_bar = WebDriverWait(page.browser, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                                                              '[class="header__button"]'))).click()
        time.sleep(10)
        sidebar_items_counter = WebDriverWait(page.browser, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                                                                      'span[class="side-menu__counter side-menu__counter--green ng-tns-c5-0 ng-star-inserted"]'))).text

        assert navbar_items_counter == sidebar_items_counter, "Side bar items counter and nav bar items counter don't " \
                                                              "match"

    #@pytest.mark.smoke
    def test_buy_status_change_to_in_basket(self, browser):
        page = MainPage(browser, link)
        page.add_to_cart(1)
        assert WebDriverWait(page.browser, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                '[class="buy-button__label ng-star-inserted"]'))).text == "В корзине", "Status hasn't changed after adding the item to a basket"

   # @pytest.mark.new
    def test_quantity_and_price_increase(self, browser):
        page = MainPage(browser, link)
        page.add_to_cart(2)
        items = page.browser.find_elements_by_css_selector('p[class="cart-product__price"]')
        items_prices = []
        for item in items:
            items_prices.append(item.text)

        items_amount_add = page.browser.find_elements_by_css_selector('button[class="button button_color_white button_size_medium cart-counter__button"][aria-label="Добавить ещё один товар"]')
        items_amount_add[0].click()
        time.sleep(12)
        # Find all items
        # Store their prices
        # Change amount of one product
        # Compare old values with new ones
        # Must change only the value that was changed on purpose
        # Check total sum change before and after
