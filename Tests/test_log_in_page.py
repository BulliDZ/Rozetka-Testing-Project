import time
import pytest
import sys
from selenium.webdriver.common.by import By
from pages.main_page import MainPage

from selenium.webdriver.support import expected_conditions as EC

link ="https://rozetka.com.ua/"

class TestLogInPage():
   # def test_log_in_successful(self, browser):
    #    try:
     #       orders_btn = None
        #    page = MainPage(browser, link)
         #   page.open()
          #  # page.click_alert()
           # log_in_btn = browser.find_element_by_css_selector(
            #"button[class='header__button ng-star-inserted'][type='button']")
            #log_in_btn.click()
            #time.sleep(2)
            #e_mail_field = page.browser.find_element_by_css_selector("#auth_email")
            #e_mail_field.send_keys("bulldogbearz@gmail.com")
            #time.sleep(2)
            #pwd_field = page.browser.find_element_by_css_selector("#auth_pass")
            #pwd_field.send_keys("AaBBCC11")
            #time.sleep(3)
            #enter_btn = page.browser.find_element_by_css_selector("[class='button button--large button--green auth-modal__submit ng-star-inserted']")
            #time.sleep(5)
            #enter_btn.click()
            # time.sleep(30000)
            #time.sleep(7)
            #orders_btn = page.browser.find_element_by_css_selector("a[class = 'header__button ng-star-inserted']")
        #finally:
         #   assert orders_btn is not None

    @pytest.mark.parametrize('e_mail', ['asdfgg', 'stuff@u.com', 'noneeedaf'])
    def test_email_field(self, browser, e_mail):
        page = MainPage(browser, link)
        page.open()
        err = None
        try:
            # page.click_alert()
            log_in_btn = page.browser.find_element_by_css_selector(
                "button[class='header__button ng-star-inserted'][type='button']")
            log_in_btn.click()
            e_mail_field = page.browser.find_element_by_css_selector("#auth_email")
            e_mail_field.send_keys(e_mail)
            time.sleep(10)
        finally:
            err = page.browser.find_element_by_css_selector('input.ng-invalid')
            assert err is None, "Invalid input"