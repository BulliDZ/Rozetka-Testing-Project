from .base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class MainPage(BasePage):

    def click_alert(self):
        main_window = self.browser.window_handles[0]
        WebDriverWait(self.browser, 5).until(EC.alert_is_present())
        alert = self.browser.switch_to.alert
        alert.accept()
        self.browser.switch_to.window(main_window)

    def find_login_button(self):
        return WebDriverWait(self.browser, 20).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, "button[class='header__button ng-star-inserted'][type='button']")))
        #return self.browser.find_element_by_css_selector("button[class='header__button ng-star-inserted'][type='button']")

    def find_register_button(self):
        return  WebDriverWait(self.browser, 20).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, "a[class='auth-modal__register-link ng-star-inserted']")))

    def go_to_register_form(self):
        self.open()
        log_in_btn = self.find_login_button()
        self.browser.execute_script("arguments[0].click()", log_in_btn)
        # log_in_btn.click()
        register_btn = self.find_register_button()
        # register_btn.click()
        self.browser.execute_script("arguments[0].click()", register_btn)

    def add_to_cart1(self):
        self.open()
        item = self.browser.find_element_by_css_selector("a[class='tile__title']")
        item.click()

        buy_btn = self.browser.find_element_by_css_selector(
            'button[class="buy-button button button_with_icon button_color_green button_size_large ng-star-inserted"]')
        buy_btn.click()

    def add_to_cart(self, amount_of_items_to_add=1):
        self.open()
        if (amount_of_items_to_add > 1):
            for i in range(0, amount_of_items_to_add):
                print(i)
                # Open category Computers
                # Select item
                # Add to a cart
                # Close cart
                # Go back
                # Select another item from the list

                if(i == 0):
                    items = self.browser.find_elements_by_css_selector("a[class='tile__title']")
                    items[0].click()
                else:
                    self.browser.execute_script("window.scrollBy(0,900);")
                    items = self.browser.find_elements_by_css_selector("a[class='lite-tile__title ng-star-inserted']")
                    #WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                     #                           "a[class='lite-tile__title ng-star-inserted']")))
                    self.browser.execute_script("arguments[0].click()", items[-2])


                buy_btn = WebDriverWait(self.browser, 20).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                    'button[class="buy-button button button_with_icon button_color_green button_size_large ng-star-inserted"]')))
                self.browser.execute_script("arguments[0].click()", buy_btn)
                close_cart_window = WebDriverWait(self.browser, 10).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                    '[class="modal__close ng-star-inserted"]')))
                self.browser.execute_script("arguments[0].click()", close_cart_window)

        else:
            item = self.browser.find_element_by_css_selector("a[class='tile__title']")
            item.click()
            buy_btn = self.browser.find_element_by_css_selector(
                'button[class="buy-button button button_with_icon button_color_green button_size_large ng-star-inserted"]')
            buy_btn.click()
            close_cart_window = WebDriverWait(self.browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, \
                                                '[class="modal__close ng-star-inserted"]'))).click()