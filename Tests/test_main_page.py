import pytest
import sys
import time
from selenium.webdriver.common.by import By
from pages.main_page import MainPage
from selenium.webdriver.support import expected_conditions as EC
link ="https://rozetka.com.ua/"

class TestMainPage:

    def test_login_link_isPresent(self, browser):
        usr_log_in_btn = None
        try:
            page = MainPage(browser, link)
            page.open()
            # page.click_alert()
            # browser.switch_to.alert.accept()
            # time.sleep(10)

            for el in browser.find_elements_by_css_selector("[class='header__button ng-star-inserted']"):
                if (el.get_attribute("type") is not None) and (el.get_attribute("type") == "button"):
                    usr_log_in_btn = el
                    break
            print("Attribute" + usr_log_in_btn.get_attribute("type"))
        except AttributeError as a:
            print(type(usr_log_in_btn))
            print(browser.find_elements_by_class_name("header__button ng-star-inserted"))
        finally:
            assert usr_log_in_btn is not None, "emty usr"


#Checks wether log in link is clickable. It passes when the browser can locate log in form
    def test_login_link_isClickable(self, browser):
        log_in_btn = None
        try:
            page = MainPage(browser, link)
            page.open()
            log_in_btn = browser.find_element_by_css_selector("button[class='header__button ng-star-inserted'][type='button']")
            log_in_btn.click()

        except Exception:
            print("Some exception occured")
           # print(browser.find_elements_by_class_name("[class='auth-modal__form ng-star-inserted ng-dirty ng-touched ng-valid']"))
        finally:
            assert log_in_btn is not None, "Log in page is not clickable!"