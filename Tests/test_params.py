import pytest
from pages.main_page import MainPage
import time

link = "https://cans.com.ua/signup/"

# if you mark.parametrize a class and its method it takes pairs from each set and runs one test at a tim
# so in here I have 6*4 = 24 tests
# in general, mark.parametrize creates tuples of parameters and uses them as entries for each test
# no matter how many mark.parametrize you got it will be just 1 entry list consisting of elements one from each parametrize

@pytest.mark.parametrize('name_property', ['firstname','lastname', 'email', 'password', 'password_confirm', 'phone'])
class TestParams:
    @pytest.mark.parametrize('name_property',
                             ['firstname', 'lastname', 'email', 'password', 'password_confirm', 'phone'])
    @pytest.mark.parametrize('field_value',['aa', 'bb'])
    def test_fields(self, browser, field_value, name_property):
        page = MainPage(browser, link)
        page.open()
        input_field = page.browser.find_element_by_css_selector('input[name="data[%s]"]'%(name_property))
        input_field.send_keys(field_value)
        time.sleep(5)