import pytest
import sys
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.main_page import MainPage


link = "https://rozetka.com.ua/"
test_len_list = [str(num) for num in range(0,100)] # 190 characters if [0;100] | 490 characters if [0;100]
test_len_list = ['a' for num in range(0, 33)]  # 190 characters if [0;100] | 490 characters if [0;200] | 90 characters if [0;50]
test_len_str = "".join(test_len_list)

def has_connection(driver):
    try:
        driver.find_element_by_xpath('//span[@jsselect="heading" and @jsvalues=".innerHTML:msg"]')
        return False
    except: return True

class TestRegisterPage():

    @pytest.mark.parametrize('name', ['Abe', 'Ыван', 'Ігор', '#22%^', 'Ыієряславё', '1ван'])
    def test_field_username_symbolics(self, name, browser):
        # is it functional testing if I need to make a sequence of steps before reaching to the element I need to test
        # Because if some step from this sequence won't work the entire test fails
        page = MainPage(browser, link)
        page.open()
        log_in_btn = page.browser.find_element_by_css_selector("button[class='header__button ng-star-inserted'][type='button']")
        log_in_btn.click()
        register_btn = page.browser.find_element_by_css_selector("a[class='auth-modal__register-link ng-star-inserted']")
        register_btn.click()
        #time.sleep(3)
        username_input = page.browser.find_element_by_css_selector("input[id='registerUserName']")
        username_input.send_keys(name)
        time.sleep(3)
        assert page.is_element_present('p[class="validation-message ng-star-inserted"]') == False, "This type of input is invalid for field_username"

    @pytest.mark.parametrize('name', ['Abe', 'Ыван', 'Ігор', '#22%^', 'Ыієряславё'])
    def test_field_username_symbolics_copy(self, name, browser):
        # is it functional testing if I need to make a sequence of steps before reaching to the element I need to test
        # Because if some step from this sequence won't work the entire test fails
        page = MainPage(browser, link)
        page.open()
        log_in_btn = WebDriverWait(page.browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "button[class='header__button ng-star-inserted'][type='button']")))
        log_in_btn.click()
        register_btn = WebDriverWait(page.browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "a[class='auth-modal__register-link ng-star-inserted']")))
        register_btn.click()
        # time.sleep(3)
        username_input = page.browser.find_element_by_css_selector("input[id='registerUserName']")
        username_input.send_keys(name)
        time.sleep(3)
        assert page.is_element_present(
            'p[class="validation-message ng-star-inserted"]') == False, "This type of input is invalid for field_username"

    @pytest.mark.len
    @pytest.mark.parametrize('num', [0, 2, 32, 512, 1024])
    def test_field_username_len(self, browser, num):
        # is it functional testing if I need to make a sequence of steps before reaching to the element I need to test
        # Because if some step from this sequence won't work the entire test fails
        # ANSWER: No, it's just unfinished test case. Functional testing refers to testing some particular functionality

        if num <= 0:
            field_value = ' '
        else:
            test_len_list = ['в' for b in range(0,
                                                num)]  # 190 characters if [0;100] | 490 characters if [0;200] | 90 characters if [0;50]
            field_value = "".join(test_len_list)


        page = MainPage(browser, link)
        page.go_to_register_form()
        # page.open()
       # log_in_btn = page.find_login_button()
        #page.browser.execute_script("arguments[0].click()", log_in_btn)
        #log_in_btn.click()
        #register_btn = page.find_register_button()
        #register_btn.click()
        # page.browser.execute_script("arguments[0].click()", register_btn)
        # time.sleep(3)
        username_input = page.browser.find_element_by_css_selector("input[id='registerUserName']")
        username_input.send_keys(field_value)
        time.sleep(3)
        assert page.is_element_present(
                'p[class="validation-message ng-star-inserted"]') == False, "Lenght does not satisfy the requirements"

    @pytest.mark.phone
    @pytest.mark.parametrize('phone', [' ', '2', '23','961481487', '+380961114447', '1234567890' ])
    def test_field_phone_should_return_the_same_value(self, browser, phone):
        try:
            page = MainPage(browser, link)
            page.go_to_register_form()
            phone_field = page.browser.find_element_by_css_selector("input[id='registerUserPhone']")
            phone_field.send_keys(phone)
            format_phone = "+380 {}{} {}{}{} {}{} {}{}".format(*phone)  # 00 XXX XX 00
            time.sleep(5)
            print(phone_field.get_attribute('value'))
            field_input_value = phone_field.get_attribute('value')
            formatted_field = ""
        except IndexError:
            assert False, "Invalid input for a phone number"

        assert (format_phone) == phone_field.get_attribute('value'), 'Field does not correspond the initial formatted output'


    @pytest.mark.mail
    @pytest.mark.parametrize('mail',['cc', '', 'jD@ff@.com','23#!@gmail.com', 'JohnDoe@gg.com', 'asfgg.co'])
    def test_mail_field_valid_input(self, mail, browser):
        page = MainPage(browser, link)
        page.go_to_register_form()
        email_field = page.browser.find_element_by_css_selector('input[id="registerUserEmail"]')
        email_field.send_keys(mail)
        time.sleep(5)
        email_field_err_message = "Введите свою эл. почту"
        for el in page.browser.find_elements_by_css_selector('p[class="validation-message ng-star-inserted"]'):
            if el.text == email_field_err_message:
                assert False, "Wrong e-mail input"
            print("1")

    @pytest.mark.pwd
    @pytest.mark.parametrize('pwd',['', '1', 'ппФррп','22333333', '22ffaa', '23FFaa!', '46TTtff'])
    def test_mail_field_valid_input(self, pwd, browser):
        page = MainPage(browser, link)
        page.go_to_register_form()
        # page.open()
        # log_in_btn = page.find_login_button()
        # page.browser.execute_script("arguments[0].click()", log_in_btn)
        # log_in_btn.click()
        # register_btn = page.find_register_button()
        # register_btn.click()
        # page.browser.execute_script("arguments[0].click()", register_btn)
        # time.sleep(3)
        password_input = page.browser.find_element_by_css_selector("input[id='registerUserPassword']")
        password_input.send_keys(pwd)

        assert page.is_element_present(
            'div[class="form__row js-new_password validation_type_error"]') == False, "Invalid password"