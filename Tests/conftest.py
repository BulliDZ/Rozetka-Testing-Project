# All the necessary fixtures for TestingOnlineShop project
import pytest
from selenium import webdriver

@pytest.fixture()
def browser():

    browser = webdriver.Chrome()
    print("Starting browser...")

    yield browser
    print("Closing browser...")
    browser.quit()


